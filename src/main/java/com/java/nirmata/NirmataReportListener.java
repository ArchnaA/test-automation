package com.java.nirmata;

import java.io.FileWriter;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.xml.XmlSuite;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.model.Test;

public class NirmataReportListener implements ITestListener{
	public static NirmataApplicationProperties  appproperties;
	
	FileWriter fileWriter;
	FileWriter fileWriterTest;
	
	@Override
	public void onTestStart(ITestResult result) {
		
			NirmataSetUp.methodInfo=NirmataSetUp.testInfo.createNode(result.getName());//.assignCategory(NirmataSetUp.testName);
		
		
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		NirmataSetUp.methodInfo.pass("Test Case Name : "+result.getName()+" is passed");
	}

	@Override
	public void onTestFailure(ITestResult result) {
		NirmataSetUp.methodInfo.log(Status.FAIL,"Test Case Name : "+result.getName()+" is failed");
		NirmataSetUp.methodInfo.log(Status.FAIL,"Test fallure : "+ result.getThrowable());
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		
		NirmataSetUp.methodInfo=NirmataSetUp.testInfo.createNode(result.getName());//.assignCategory(NirmataSetUp.testName);
		NirmataSetUp.methodInfo.log(Status.SKIP,"Test Case Name : "+result.getName()+" is skiped");
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		//System.out.println("onTestFailedButWithinSuccessPercentage");
	}

	@Override
	public void onStart(ITestContext context) {
		appproperties=new NirmataApplicationProperties();
		NirmataSetUp.testInfo=NirmataSetUp.suiteInfo.createNode(context.getName());
		NirmataSetUp.testName=context.getName();
		
	}

	@Override
	public void onFinish(ITestContext context) {
		if(context.getFailedConfigurations().size() > 0 || context.getFailedTests().size() > 0)
		{
			try {
				fileWriter = new FileWriter("Report/failedSubject.txt",false);
				fileWriter.write("FAILURE\n"); 
				fileWriter.close();		
			} catch (Exception e) {
				e.printStackTrace();
				 
			}	
			
			try {
				fileWriterTest = new FileWriter("Report/failedMethods.txt",true);
				fileWriterTest.write("Test Case "+context.getName()+" Failed.\n"); 
				fileWriterTest.close();		
			} catch (Exception e) {
				e.printStackTrace();
				
			}	
		}
		else {
			try {
				fileWriter = new FileWriter("Report/failedSubject.txt",false);
				fileWriter.write("SUCCESS\n"); 
				fileWriter.close();		
			} catch (Exception e) {
				e.printStackTrace();
				
			}	
			
		}
	}

}
