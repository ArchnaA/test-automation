package com.java.nirmata.signup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class Signup {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	private ExtentTest methodInfo=NirmataSetUp.methodInfo;
	@BeforeClass
	public void SetupDriver()
	{

		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	@Test(priority=1,testName= "Open Signup Page")
	public void openSignupPage()
	{
		webDriver.get(appproperties.properties.getProperty("SignUpURL"));
		String title= webDriver.getTitle();
		Assert.assertEquals(title, appproperties.properties.getProperty("SignUpPageTitle"));
	}
	@Test(priority=2,testName= "Verify Signup Page",dependsOnMethods= {"openSignupPage"})
	public void verifySignupPage()
	{
		WebElement signuppage = webDriver.findElement(By.className("login-title"));
		String signupPagetitel= signuppage.getText();
		Assert.assertEquals(signupPagetitel, appproperties.properties.getProperty("SignupText"));
	}
	@Test(priority=2,testName= "Set Data For SignUp",dependsOnMethods= {"verifySignupPage"})
	public void fillDataForSignup()
	{
		webDriver.findElement(By.id("name")).sendKeys(appproperties.properties.getProperty("TestName"));
		webDriver.findElement(By.id("email")).sendKeys(appproperties.properties.getProperty("TestEmailID"));
		webDriver.findElement(By.id("company")).sendKeys(appproperties.properties.getProperty("TestCompanyName"));
		webDriver.findElement(By.id("phone")).sendKeys(appproperties.properties.getProperty("TestPhoneNo"));
		
		//webDriver.findElement(By.xpath("//button[@id='btnSignupEmail']")).click();
	}
}
