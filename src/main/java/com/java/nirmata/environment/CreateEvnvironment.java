package com.java.nirmata.environment;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class CreateEvnvironment {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	@Test(priority=6,testName= "Click on Environment Menu")
	public void clickEnvironment() throws InterruptedException
	{
		WebElement clusterMenu=webDriver.findElement(By.id("env_menu"));
		clusterMenu.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addEnvironment")));
	}
	@Test(priority=7,testName= "Click on Add Environment",dependsOnMethods= {"clickEnvironment"})
	public void clickAddEnvironment() throws InterruptedException
	{
		WebElement AddClusters=webDriver.findElement(By.id("addEnvironment"));
		AddClusters.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 200);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='modal-dialog']")));
	}
	@Test(priority=8,testName= "Fill Environment Data",dependsOnMethods= {"clickAddEnvironment"})
	public void fillEnvironmentData() throws InterruptedException
	{
		WebElement EnvName = webDriver.findElement(By.xpath("//*[@id=\"name\"]"));
		EnvName.sendKeys(appproperties.properties.getProperty("EnvName"));
		
		Select Cluster = new Select(webDriver.findElement(By.name("hostCluster")));
		Cluster.selectByVisibleText(appproperties.properties.getProperty("ClusterName"));
		
		Select isolationLevel = new Select(webDriver.findElement(By.name("isolationLevel")));
		isolationLevel.selectByValue(appproperties.properties.getProperty("IsolatedLevel"));
		
		webDriver.findElement(By.xpath("//button[@class='btn btn-primary btn-nirmata']")).click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 200);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='modal-dialog']")));
	}
	@Test(priority=9,testName= "Wait for Environment Create",dependsOnMethods= {"fillEnvironmentData"})
	public void waitForEnvironmentCreate() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(webDriver, 200);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("state-popover")));
		boolean status=true;
		Date currentDate=new Date();
		do
		{
			WebElement statusText = webDriver.findElement(By.id("state-popover"));
			if(statusText.getText().contains("Cluster Connected"))
			{
				status=false;
			}
			else if(statusText.getText().contains("Cluster Not Connected"))
			{
				status=false;
				Assert.fail("Environment failed : "+statusText.getText());
			}
			long diff =  new Date().getTime()  - currentDate.getTime();
			long diffMinutes = diff / (60 * 1000) % 60;
			//System.out.println("Wait Time :"+diffMinutes);
			if(diffMinutes>5)
			{
				status=false;
				Assert.fail("Environment failed : "+statusText.getText());
				
			}
			Thread.sleep(10000);
			
		}while(status);
	}
}
