package com.java.nirmata.clusters;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;
import com.java.nirmata.hostgroups.RunSystemScripts;

public class CreateClusters {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	@Test(priority=6,testName= "Click on Clusters Menu")
	public void clickClusters() throws InterruptedException
	{
		WebElement clusterMenu=webDriver.findElement(By.id("hostClusters_menu"));
		clusterMenu.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 900);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
	}
	@Test(priority=7,testName= "Click on Add Clusters",dependsOnMethods= {"clickClusters"})
	public void clickHostGroup() throws InterruptedException
	{
		WebElement AddClusters=webDriver.findElement(By.id("addCluster"));
		AddClusters.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 900);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='text-center dialog-title-lg']")));
	}
	
	@Test(priority=8,testName= "Click on No - Install and manage Kubernetes for me",dependsOnMethods= {"clickHostGroup"})
	public void clickInstallandmanageKubernetes() throws InterruptedException
	{
		
		WebElement NoInstall=webDriver.findElement(By.xpath("//div[@data-id='managed']"));
		NoInstall.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 900);
		wait.until(ExpectedConditions.attributeContains(webDriver.findElement(By.id("k8sClusterManageType-tab")), "class", "done"));
	}
	@Test(priority=9,testName= "Filling cluster data",dependsOnMethods= {"clickInstallandmanageKubernetes"})
	public void fillingcreateClusterData() throws InterruptedException
	{
		
		List<WebElement> Name = webDriver.findElements(By.xpath("//*[@id=\"name\"]"));
		for(int i=0;i<Name.size();i++)
		{
			if(Name.get(i).isDisplayed())
			{
				Name.get(i).sendKeys(appproperties.properties.getProperty("ClusterName"));
			}
		}
		
		Select HostGroups = new Select(webDriver.findElement(By.name("hostGroups")));
		HostGroups.selectByVisibleText(appproperties.properties.getProperty("HostGroupName"));
		
		Select Policy = new Select(webDriver.findElement(By.name("policySelector")));
		Policy.selectByValue(appproperties.properties.getProperty("SelectPolicy"));
		
		webDriver.findElement(By.xpath("//button[@class='btn btn-next btn-nirmata btn-step']")).click();
		
		
		WebDriverWait wait = new WebDriverWait(webDriver, 900);
		wait.until(ExpectedConditions.attributeContains(webDriver.findElement(By.id("k8sManagedClusterFinish")), "class", "active"));
	}
	@Test(priority=10,testName= "Waiting For Cluster Create",dependsOnMethods= {"fillingcreateClusterData"})
	public void waitingForClusterCreate() throws InterruptedException
	{
		List<WebElement> closeButton = webDriver.findElements(By.id("close-button"));
		WebElement modalClose=null;
		for(int i=0;i<closeButton.size();i++)
		{
			
			if(closeButton.get(i).isDisplayed())
			{
				modalClose=closeButton.get(i);
				closeButton.get(i).click();
				break;
				
			}
		}
		
		WebDriverWait wait = new WebDriverWait(webDriver, 900);
		wait.until(ExpectedConditions.invisibilityOf(modalClose));
		String ClusterName=appproperties.properties.getProperty("ClusterName");
		WebElement SpecificCluster=webDriver.findElement(By.xpath("//*[contains(text(), '"+ClusterName+"')]"));
		SpecificCluster.click();
		wait = new WebDriverWait(webDriver, 900);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("state-popover")));
		Date currentDate=new Date();
		boolean status=true;
		int countFailedState=0;
		do
		{
			try
			{
					WebElement statusText= webDriver.findElement(By.id("state-popover"));	
					if(statusText.getText().contains("Ready"))
					{
						status=false;
					}
					else if(statusText.getText().contains("Failed"))
					{
						countFailedState++;
						if(countFailedState > 10)
						{
							status=false;
							Assert.fail("Cluster failed : "+statusText.getText());
						}
					}
					else
					{
						countFailedState=0;
					}
					long diff =  new Date().getTime()  - currentDate.getTime();
					long diffMinutes = diff / (60 * 1000) % 60;
					//System.out.println("Wait Time For Cluster Creation:"+diffMinutes);
					if(diffMinutes>10)
					{
						status=false;
						Assert.fail("Cluster failed : "+statusText.getText());
						
					}
					Thread.sleep(10000);
					
			}catch(Exception e)
			{
				
			}
			
		}while(status);
		
	}
}
