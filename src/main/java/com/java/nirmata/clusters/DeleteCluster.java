package com.java.nirmata.clusters;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;
import com.java.nirmata.hostgroups.RunSystemScripts;

public class DeleteCluster {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	@Test(priority=6,testName= "CleanUp Cluster")
	public void cleanUpCluster() throws IOException
	{
		new RunSystemScripts().runExec("sudo service nirmata-agent stop");
		new RunSystemScripts().runExec("kubectl delete pod --all");
		new RunSystemScripts().runExec("kubectl delete deployment --all");
		new RunSystemScripts().runExec("kubectl delete service --all");
		new RunSystemScripts().runExec("kubectl delete role --all");
		new RunSystemScripts().runExec("kubectl delete rolebinding --all");
		new RunSystemScripts().runExec("kubectl delete sa --all");
		new RunSystemScripts().runExec("kubectl delete secret --all");
		new RunSystemScripts().runExec("kubectl delete --all namespaces --grace-period=0 --force");
		new RunSystemScripts().runExec("sudo docker stop $(sudo docker ps -a -q)");
		new RunSystemScripts().runExec("sudo docker rm $(sudo docker ps -a -q)");
		new RunSystemScripts().runExec("sudo docker rmi $(sudo docker images -a -q)");
		new RunSystemScripts().runExec("sudo docker rmi -f `sudo docker images -a -q`");
		new RunSystemScripts().runExec("sudo service nirmata-agent start");
		
	}
	@Test(priority=7,testName= "Click on Clusters Menu")
	public void clickClusters()
	{
		WebElement clusterMenu=webDriver.findElement(By.id("hostClusters_menu"));
		clusterMenu.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
	}
	@Test(priority=8,testName= "Cluster Details",dependsOnMethods= {"clickClusters"})
	public void ClusterDetails()
	{
		String ClusterName=appproperties.properties.getProperty("ClusterName");
		WebElement SpecificCluster=webDriver.findElement(By.xpath("//*[contains(text(), '"+ClusterName+"')]"));
		SpecificCluster.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("state-popover")));
	}
	@Test(priority=9,testName= "Click on Setting Button",dependsOnMethods= {"ClusterDetails"})
	public void clickOnSettingButton()
	{
		WebElement SettingButton=webDriver.findElement(By.id("model-action-button"));
		SettingButton.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("editHostCluster")));
	}
	@Test(priority=10,testName= "Click on Delete Button",dependsOnMethods= {"clickOnSettingButton"})
	public void clickOnDeleteButton()
	{
		WebElement DeleteButton=webDriver.findElement(By.id("deleteHostCluster"));
		DeleteButton.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn btn-danger']")));
	}
	@Test(priority=11,testName= "Delete Cluster",dependsOnMethods= {"clickOnDeleteButton"})
	public void deleteCluster() throws InterruptedException
	{
		webDriver.findElement(By.id("name")).sendKeys(appproperties.properties.getProperty("ClusterName"));
		webDriver.findElement(By.xpath("//button[@class='btn btn-danger']")).click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 200);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addCluster")));
		String ClusterName=appproperties.properties.getProperty("ClusterName");
		wait = new WebDriverWait(webDriver, 200);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(text(), '"+ClusterName+"')]")));
		
	}
}
