package com.java.nirmata.application;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class CreateApplication {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	@Test(priority=6,testName= "Click on Application Menu")
	public void clickApplication() throws InterruptedException
	{
		WebElement clusterMenu=webDriver.findElement(By.id("applications_menu"));
		clusterMenu.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(By.id("currentCatalog_menu"))));
		
		WebElement application=webDriver.findElement(By.id("currentCatalog_menu"));
		application.click();
		
		wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addApplication")));
		
	}
	@Test(priority=7,testName= "Click on Add Application",dependsOnMethods= {"clickApplication"})
	public void clickOnAddApplication() throws InterruptedException
	{
		WebElement addApplication=webDriver.findElement(By.id("addApplication"));
		addApplication.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(), 'Create Application')]")));
	}
	@Test(priority=8,testName= "Fill Application Data",dependsOnMethods= {"clickOnAddApplication"})
	public void fillApplicationData() throws InterruptedException
	{
		WebElement appname=webDriver.findElement(By.id("name"));
		appname.sendKeys(appproperties.properties.getProperty("AppName"));
		
		  WebElement uploadElement = webDriver.findElement(By.className("dz-hidden-input"));
		  uploadElement.sendKeys("/mnt1/jenkins/workspace/RELEASE_2.3.0/Automation/Test-Automation/driver/Deployment-hello-worldDeployment.yml");
	}
	@Test(priority=9,testName= "Wait For Application Create",dependsOnMethods= {"fillApplicationData"})
	public void waitForApplicationcreate() throws InterruptedException
	{
		webDriver.findElement(By.xpath("//button[@class='btn btn-primary btn-nirmata']")).click();
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.invisibilityOf(webDriver.findElement(By.xpath("//*[@class='modal-backdrop fade in']"))));
		
		wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("controls")));
	}
}
