package com.java.nirmata.application;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class DeleteDeployApplication {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	@Test(priority=6,testName= "Click on Environment Menu")
	public void clickEnvironmentMenu() throws InterruptedException
	{
		WebElement clusterMenu=webDriver.findElement(By.id("env_menu"));
		clusterMenu.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 200);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("environment-info")));
		
	}
	@Test(priority=7,testName= "Click on Specific Environment",dependsOnMethods= {"clickEnvironmentMenu"})
	public void clickonSpecificEnvironment() throws InterruptedException
	{
		String EnvName=appproperties.properties.getProperty("EnvName");
		WebElement SpecificApp=webDriver.findElement(By.xpath("//*[contains(text(), '"+EnvName+"')]"));
		SpecificApp.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("application-info")));
	}
	@Test(priority=8,testName= "Click on Specific Deploy Application",dependsOnMethods= {"clickonSpecificEnvironment"})
	public void clickonSpecificDeployApplication() throws InterruptedException
	{
		String DeployAppName=appproperties.properties.getProperty("RunAppName");
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(), '"+DeployAppName+"')]")));
		
		WebElement SpecificDeployApp=webDriver.findElement(By.xpath("//*[contains(text(), '"+DeployAppName+"')]"));
		SpecificDeployApp.click();
		
		wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-action-button")));
	}
	@Test(priority=9,testName= "Click on Action Button",dependsOnMethods= {"clickonSpecificDeployApplication"})
	public void clickonActionButton() throws InterruptedException
	{
		WebElement actionButton=webDriver.findElement(By.id("model-action-button"));
		actionButton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("deleteApplication")));
	}
	@Test(priority=10,testName= "Click on Delete Button",dependsOnMethods= {"clickonActionButton"})
	public void clickonDeleteButton() throws InterruptedException
	{
		WebElement deleteButton=webDriver.findElement(By.id("deleteApplication"));
		deleteButton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn btn-danger']")));
	}
	@Test(priority=11,testName= "Delete Button Application",dependsOnMethods= {"clickonDeleteButton"})
	public void deleteApplication() throws InterruptedException
	{
		webDriver.findElement(By.id("run")).sendKeys(appproperties.properties.getProperty("RunAppName"));
		webDriver.findElement(By.xpath("//button[@class='btn btn-danger']")).click();
		boolean status=true;
		Date currentDate=new Date();
		do
		{
			if(webDriver.findElements(By.id("application-info")).size()!=0)
			{
				status=false;
			}
			long diff =  new Date().getTime()  - currentDate.getTime();
			long diffMinutes = diff / (60 * 1000) % 60;
			//System.out.println("Wait Time For Delete Deployed Application :"+diffMinutes);
			if(diffMinutes>5)
			{
				status=false;
			}
			Thread.sleep(10000);
			
		}while(status);
		
	
	}
}
