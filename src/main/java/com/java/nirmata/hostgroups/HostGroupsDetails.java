package com.java.nirmata.hostgroups;

import java.lang.reflect.Method;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class HostGroupsDetails{

	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	private ExtentTest testInfo;
	@BeforeMethod
	public void reportStart(Method method)
	{
		
		
		String testName=method.getName();
		testInfo=NirmataSetUp.reports.createTest(testName);
		
		
	}
	
	@AfterMethod
	public void reportEnd(ITestResult result)
	{
		if(result.getStatus()==ITestResult.SUCCESS)
		{
			testInfo.log(Status.PASS,"Test Case Name : "+result.getName()+" is passed");
		}
		else if(result.getStatus()==ITestResult.FAILURE)
		{
			testInfo.log(Status.PASS,"Test Case Name : "+result.getName()+" is failed");
			testInfo.log(Status.FAIL,"Test fallure : "+ result.getThrowable());
		}
		else if(result.getStatus()==ITestResult.SKIP)
		{
			testInfo.log(Status.SKIP,"Test Case Name : "+result.getName()+" is skiped");
		}
	}
	
	@Test(priority=6,testName= "Setup a WebDriver")
	public void setupDriver()
	{
		
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	
	@Test(priority=7,testName= "Clicking on Host Group Menu")
	public void clickHostGroup() throws InterruptedException
	{
		WebElement HomeGroupMenu=webDriver.findElement(By.id("hostgroups_menu"));
		HomeGroupMenu.click();
		
	}
	
	@Test(priority=8,dependsOnMethods= {"clickHostGroup"},testName= "Clicking on AWS Service Menu")
	public void clickOnAmazonWebService()
	{
		WebElement HostGroupMenu=webDriver.findElement(By.id("aws_menu"));
		HostGroupMenu.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
		
		WebElement AWSHostGroups = webDriver.findElement(By.id("model-content-panel-title"));
		String AWSHostGroups_str= AWSHostGroups.getText();
		Assert.assertEquals(AWSHostGroups_str, appproperties.properties.getProperty("AWSHostGroups"));
	}
	
	@Test(priority=9,dependsOnMethods= {"clickOnAmazonWebService"},testName= "Reading Number Of AWS Service Present and there status")
	public void awsService()
	{
		List<WebElement> ListOfServices = webDriver.findElements(By.className("card-content"));
		
		testInfo.info("Total number of AWS Service:- "+ListOfServices.size());
		for(int i=0;i<ListOfServices.size();i++)
		{
			
			
			WebElement Title=ListOfServices.get(i).findElement(By.className("card-title"));
			WebElement status=ListOfServices.get(i).findElement(By.className("card-status"));
			testInfo.info("AWS Service Name:- "+Title.getText());
			testInfo.info("AWS Service Status:- "+status.getText());
			
		}
		
		
	}
	
	@Test(priority=10,dependsOnMethods= {"clickHostGroup"},testName= "Click on Microsoft Azure Menu")
	public void clickOnMicrosoftAzure()
	{
		WebElement MicrosoftAzurebutton=webDriver.findElement(By.id("azure_menu"));
		MicrosoftAzurebutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
		
		WebElement MicrosoftAzurePage = webDriver.findElement(By.id("model-content-panel-title"));
		String MicrosoftAzurePage_title= MicrosoftAzurePage.getText();
		Assert.assertEquals(MicrosoftAzurePage_title, appproperties.properties.getProperty("MicrosoftAzure"));
	}
	
	@Test(priority=11,dependsOnMethods= {"clickOnMicrosoftAzure"},testName= "Reading Number of Microsoft Azure service present and there status")
	public void microsoftAzureService()
	{
		
		
		
		List<WebElement> ListOfServices = webDriver.findElements(By.className("card-content"));
		
		testInfo.info("Total number of Microsoft Azure Service:- "+ListOfServices.size());
		for(int i=0;i<ListOfServices.size();i++)
		{
			
			
			WebElement Title=ListOfServices.get(i).findElement(By.className("card-title"));
			WebElement status=ListOfServices.get(i).findElement(By.className("card-status"));
			testInfo.info("Microsoft Azure Service Name:- "+Title.getText());
			testInfo.info("Microsoft Azure Service Status:- "+status.getText());
			
		}
		
	}
	
	@Test(priority=12,dependsOnMethods= {"clickHostGroup"})
	public void clickOnGoogleCloudPlatfrom()
	{
		WebElement GoogleCloudPlatfrombutton=webDriver.findElement(By.id("googlecloudplatform_menu"));
		GoogleCloudPlatfrombutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
		
		WebElement GoogleCloudPlatfrombuttonPage = webDriver.findElement(By.id("model-content-panel-title"));
		String GoogleCloudPlatfrombuttonPage_title= GoogleCloudPlatfrombuttonPage.getText();
		Assert.assertEquals(GoogleCloudPlatfrombuttonPage_title, appproperties.properties.getProperty("GoogleCloudPlatfrom"));
	}
	
	@Test(priority=13,dependsOnMethods= {"clickOnGoogleCloudPlatfrom"})
	public void googleCloudPlatfromService()
	{
		
		
		
		List<WebElement> ListOfServices = webDriver.findElements(By.className("card-content"));
		
		testInfo.info("Total number of Google Cloud Platfrom Service:- "+ListOfServices.size());
		for(int i=0;i<ListOfServices.size();i++)
		{
			
			
			WebElement Title=ListOfServices.get(i).findElement(By.className("card-title"));
			WebElement status=ListOfServices.get(i).findElement(By.className("card-status"));
			testInfo.info("Google Cloud Platfrom Service Name:- "+Title.getText());
			testInfo.info("Google Cloud Platfrom Service Status:- "+status.getText());
			
		}
		
	}
	
	@Test(priority=14,dependsOnMethods= {"clickHostGroup"})
	public void clickOnDiamanti()
	{
		WebElement Diamantibutton=webDriver.findElement(By.id("diamanti_menu"));
		Diamantibutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
		
		WebElement DiamantibuttonPage = webDriver.findElement(By.id("model-content-panel-title"));
		String DiamantibuttonPage_title= DiamantibuttonPage.getText();
		Assert.assertEquals(DiamantibuttonPage_title, appproperties.properties.getProperty("Diamanti"));
	}
	
	@Test(priority=15,dependsOnMethods= {"clickOnDiamanti"})
	public void diamantiService()
	{
		
		
		List<WebElement> ListOfServices = webDriver.findElements(By.className("card-content"));
		
		testInfo.info("Total number of Diamanti Service:- "+ListOfServices.size());
		for(int i=0;i<ListOfServices.size();i++)
		{
			
			
			WebElement Title=ListOfServices.get(i).findElement(By.className("card-title"));
			WebElement status=ListOfServices.get(i).findElement(By.className("card-status"));
			testInfo.info("Diamanti Service Name:- "+Title.getText());
			testInfo.info("Diamanti Service Status:- "+status.getText());
			
		}
	}
	
	
	@Test(priority=16,dependsOnMethods= {"clickHostGroup"})
	public void clickOnDirectConnect()
	{
		WebElement DirectConnectbutton=webDriver.findElement(By.id("other_host_groups_menu"));
		DirectConnectbutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
		
		WebElement DirectConnectPage = webDriver.findElement(By.id("model-content-panel-title"));
		String DirectConnectPage_title= DirectConnectPage.getText();
		Assert.assertEquals(DirectConnectPage_title, appproperties.properties.getProperty("DirectConnect"));
	}
	
	@Test(priority=17,dependsOnMethods= {"clickOnDirectConnect"})
	public void directConnectService()
	{
		
		
		List<WebElement> ListOfServices = webDriver.findElements(By.className("card-content"));
		
		testInfo.info("Total number of Direct Connect Service:- "+ListOfServices.size());
		for(int i=0;i<ListOfServices.size();i++)
		{
			
			
			WebElement Title=ListOfServices.get(i).findElement(By.className("card-title"));
			WebElement status=ListOfServices.get(i).findElement(By.className("card-status"));
			testInfo.info("Direct Connect Service Name:- "+Title.getText());
			testInfo.info("Direct Connect Service Status:- "+status.getText());
			
		}
	}
	
//	@Test(priority=18,dependsOnMethods= {"clickHostGroup"})
//	public void clickOnAllHosts()
//	{
//		WebElement AllHostbutton=webDriver.findElement(By.id("all_hosts_menu"));
//		AllHostbutton.click();
//		
//		WebDriverWait wait = new WebDriverWait(webDriver, 40);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
//		
//		WebElement AllHostbuttonPage = webDriver.findElement(By.id("model-content-panel-title"));
//		String AllHostbutton_title= AllHostbuttonPage.getText();
//		Assert.assertEquals(AllHostbutton_title, appproperties.properties.getProperty("AllHosts"));
//	}
//	
//	@Test(priority=19,dependsOnMethods= {"clickOnAllHosts"})
//	public void allHostService()
//	{
//		Reporter.log("----------------------------------");
//		
//		List<WebElement> ListOfServices = webDriver.findElements(By.xpath("//div[@class='col-md-12 model-table-container']/table/tbody/tr"));
//		
//		Reporter.log("Total number of Host Service:- "+ListOfServices.size());
//		for(int i=0;i<ListOfServices.size();i++)
//		{
//			Reporter.log("----------------------------------");
//			
//			List<WebElement> ListOfData=ListOfServices.get(i).findElements(By.xpath("//td"));
//			
//			for(int j=0;j<ListOfData.size();j++)
//			{
//				switch(j)
//				{
//				   case 0:
//					   Reporter.log("Name:- "+ListOfData.get(j).getText());
//					   break;
//				   case 1:
//					   Reporter.log("Labels:- "+ListOfData.get(j).getText());
//					   break;
//				   case 2:
//					   Reporter.log("Host Group:- "+ListOfData.get(j).getText());
//					   break;
//				   case 3:
//					   Reporter.log("Agent Version:- "+ListOfData.get(j).getText());
//					   break;
//				   case 4:
//					   Reporter.log("Docker:- "+ListOfData.get(j).getText());
//					   break;
//				   case 5:
//					   Reporter.log("IP Addresses:- "+ListOfData.get(j).getText());
//					   break;
//				   case 6:
//					   Reporter.log("Memory(MB):- "+ListOfData.get(j).getText());
//					   break;
//				   case 7:
//					   Reporter.log("Containers:- "+ListOfData.get(j).getText());
//					   break;
//				   case 8:
//					   Reporter.log("State:- "+ListOfData.get(j).getText());
//					   break;
//				   default:
//						   break;
//					   
//				}
//					
//					
//				
//			}
//			
//		}
//	}
//	
//	@Test(priority=20,dependsOnMethods= {"clickHostGroup"})
//	public void clickOnAllContainers()
//	{
//		WebElement AllContainersbutton=webDriver.findElement(By.id("all_containers_menu"));
//		AllContainersbutton.click();
//		
//		WebDriverWait wait = new WebDriverWait(webDriver, 40);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
//		
//		WebElement AllContainersbuttonPage = webDriver.findElement(By.id("model-content-panel-title"));
//		String AllContainersbuttonPage_title= AllContainersbuttonPage.getText();
//		Assert.assertEquals(AllContainersbuttonPage_title, appproperties.properties.getProperty("AllContainers"));
//	}
//	
//	@Test(priority=21,dependsOnMethods= {"clickOnAllContainers"})
//	public void allContainersService()
//	{
//		Reporter.log("----------------------------------");
//		
//		List<WebElement> ListOfServices = webDriver.findElements(By.xpath("//div[@class='col-md-12 model-table-container']/table/tbody/tr"));
//		
//		Reporter.log("Total number of Containers Service:- "+ListOfServices.size());
//		
//		for(int i=0;i<ListOfServices.size();i++)
//		{
//			Reporter.log("----------------------------------");
//			
//			List<WebElement> ListOfData=ListOfServices.get(i).findElements(By.xpath("//td"));
//			
//			for(int j=0;j<ListOfData.size();j++)
//			{
//				switch(j)
//				{
//				   case 0:
//					   Reporter.log("Container Id:- "+ListOfData.get(j).getText());
//					   break;
//				   case 1:
//					   Reporter.log("Name:- "+ListOfData.get(j).getText());
//					   break;
//				   case 2:
//					   Reporter.log("Service:- "+ListOfData.get(j).getText());
//					   break;
//				   case 3:
//					   Reporter.log("Environment:- "+ListOfData.get(j).getText());
//					   break;
//				   case 4:
//					   Reporter.log("Parent Name:- "+ListOfData.get(j).getText());
//					   break;
//				   case 5:
//					   Reporter.log("Cloud:- "+ListOfData.get(j).getText());
//					   break;
//				   case 6:
//					   Reporter.log("Container IP:- "+ListOfData.get(j).getText());
//					   break;
//				   case 7:
//					   Reporter.log("State:- "+ListOfData.get(j).getText());
//					   break;
//				   default:
//						   break;
//					   
//				}
//					
//					
//				
//			}
//			
//		}
//		
//		
//	}
	
	
}
