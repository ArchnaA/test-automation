package com.java.nirmata.hostgroups;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class CreateDirectConnectHostGroups {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
		
	}
	@Test(priority=6,testName= "Click on Host Group Menu")
	public void clickHostGroup() throws InterruptedException
	{
		WebElement HomeGroupMenu=webDriver.findElement(By.id("hostgroups_menu"));
		HomeGroupMenu.click();
		
	}

	@Test(priority = 7, dependsOnMethods = { "clickHostGroup" }, testName = "Click on Direct Connect Menu")
	public void clickOnDirectConnect() {
		WebElement DirectConnectbutton = webDriver.findElement(By.id("other_host_groups_menu"));
		DirectConnectbutton.click();

		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));

		WebElement DirectConnectPage = webDriver.findElement(By.id("model-content-panel-title"));
		String DirectConnectPage_title = DirectConnectPage.getText();
		Assert.assertEquals(DirectConnectPage_title, appproperties.properties.getProperty("DirectConnect"));
	}
	@Test(priority=8,dependsOnMethods= {"clickOnDirectConnect"},testName= "Click on add host group button")
	public void clickAddHostGroup()
	{
		WebElement AddHostGroupbutton=webDriver.findElement(By.id("addHostGroup"));
		AddHostGroupbutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("bootstrap-dialog-title")));
		
		WebElement AddHostGroupDialog = webDriver.findElement(By.className("bootstrap-dialog-title"));
		String AddHostGroupDialog_title= AddHostGroupDialog.getText();
		Assert.assertEquals(AddHostGroupDialog_title, appproperties.properties.getProperty("DirectConnectHostTitle"));
	}
	@Test(priority=10,dependsOnMethods= {"clickAddHostGroup"},testName= "Set data in Host Group tab")
	public void fillinghostGroupTabData()
	{
		webDriver.findElement(By.id("name")).sendKeys(appproperties.properties.getProperty("HostGroupName"));
		
		WebElement wizarderror = webDriver.findElement(By.xpath("//div[@class='text-danger text-center']"));
		String wizarderror_text= wizarderror.getText();
		
		if(wizarderror_text!=null && !wizarderror_text.equals(""))
		{
			Assert.fail(wizarderror_text);
		}
		else
		{
			NirmataSetUp.methodInfo.info("Host Group Name:- "+appproperties.properties.getProperty("HostGroupName"));
			webDriver.findElement(By.xpath("//button[@class='btn btn-finish btn-nirmata btn-default btn-primary']")).click();
			WebDriverWait wait = new WebDriverWait(webDriver, 40);
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@id=\"hosts-table\"]/div/pre[2]")));
		}
	}
	@Test(priority=11,dependsOnMethods= {"fillinghostGroupTabData"},testName= "CleanUp Instance")
	public void cleanInstance() throws InterruptedException, IOException
	{
		
		new RunSystemScripts().runExec("sudo service nirmata-agent stop");
		new RunSystemScripts().runExec("sudo docker stop $(sudo docker ps -a -q)");
		new RunSystemScripts().runExec("sudo docker rm $(sudo docker ps -a -q)");
		new RunSystemScripts().runExec("sudo docker rmi $(sudo docker images -a -q)");
		new RunSystemScripts().runExec("sudo docker rmi -f `sudo docker images -a -q`");
		
		
	}
	@Test(priority=12,dependsOnMethods= {"cleanInstance"},testName= "Waiting till Host Group created")
	public void waitingTillHostGroupCreated() throws InterruptedException, IOException
	{
		WebElement DockerCommand = webDriver.findElement(By.xpath("//*[@id=\"hosts-table\"]/div/pre[2]"));
		String DockerCommand_text= DockerCommand.getText();
		new RunSystemScripts().runExec("sudo service nirmata-agent stop");
		new RunSystemScripts().runExec(DockerCommand_text);
		
		WebDriverWait wait = new WebDriverWait(webDriver, 500);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("state")));
		if(!webDriver.findElement(By.id("state")).getText().equalsIgnoreCase("connected"))
		{
			Assert.fail(webDriver.findElement(By.id("state")).getText());
		}
		
	}
	
}
