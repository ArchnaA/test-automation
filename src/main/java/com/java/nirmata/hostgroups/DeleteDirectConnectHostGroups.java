package com.java.nirmata.hostgroups;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class DeleteDirectConnectHostGroups {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	@Test(priority=6,testName= "Click on Host Group Menu")
	public void clickHostGroup()
	{
		WebElement HomeGroupMenu=webDriver.findElement(By.id("hostgroups_menu"));
		HomeGroupMenu.click();
		
	}
	@Test(priority=7,dependsOnMethods= {"clickHostGroup"},testName= "Click on Direct Connect Menu")
	public void clickOnDirectConnect()
	{
		WebElement DirectConnectbutton=webDriver.findElement(By.id("other_host_groups_menu"));
		DirectConnectbutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
		
		WebElement DirectConnectPage = webDriver.findElement(By.id("model-content-panel-title"));
		String DirectConnectPage_title= DirectConnectPage.getText();
		Assert.assertEquals(DirectConnectPage_title, appproperties.properties.getProperty("DirectConnect"));
	}
	@Test(priority=8,dependsOnMethods= {"clickOnDirectConnect"},testName= "Host Group Details")
	public void clickForHostGroupDetails()
	{
		
		WebElement SpecificHostGroup=webDriver.findElement(By.xpath("//*[contains(text(), '"+appproperties.properties.getProperty("HostGroupName")+"')]"));
		SpecificHostGroup.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("state-popover")));
	}
	@Test(priority=9,testName= "Click on Setting Button",dependsOnMethods= {"clickForHostGroupDetails"})
	public void clickOnSettingButton()
	{
		WebElement SettingButton=webDriver.findElement(By.id("model-action-button"));
		SettingButton.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("deleteHostGroup")));
	}
	@Test(priority=10,testName= "Click on Delete Button",dependsOnMethods= {"clickOnSettingButton"})
	public void clickOnDeleteButton()
	{
		WebElement DeleteButton=webDriver.findElement(By.id("deleteHostGroup"));
		DeleteButton.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn btn-danger']")));
	}
	@Test(priority=11,testName= "Delete Cluster",dependsOnMethods= {"clickOnDeleteButton"})
	public void deleteCluster()
	{
		webDriver.findElement(By.xpath("//button[@class='btn btn-danger']")).click();
		
	}
}
