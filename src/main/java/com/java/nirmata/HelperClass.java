package com.java.nirmata;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HelperClass {

	public String getCode() throws IOException
	{
		String code=null;
		 URL url=null;
		 HttpURLConnection connection=null;
		try {
			url = new URL("https://bitbucket.org/mrutyunjaya_neualto/test-automation/src/master/");
			connection = (HttpURLConnection) url.openConnection();
		    connection.setRequestMethod("GET");
		    connection.setDoInput(true);
		    BufferedReader in = new BufferedReader( new InputStreamReader(connection.getInputStream()));
			 
			 String codeFromURL=null;
			 while ((codeFromURL = in.readLine()) != null) {
				 if(codeFromURL.contains("VerificationCode"))
				 {
					 ////System.out.println("Code : "+codeFromURL);
					String temp= codeFromURL.replaceAll("\\<.*?\\>", "");
					 
					 //System.out.println("Code : "+temp.split(":")[1].toString());
					 code=temp.split(":")[1].toString();
				 }
			 }
			 in.close();
			 
			 connection.disconnect();
		} catch (Exception e) {
			
			
		}
		
		
		return code;
	}
}
